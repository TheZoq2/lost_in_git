# Lost in git

A game played in git.

To play, you need to install some commit hooks to the local repo:
```
./install_hooks
```

Now, run
```
git checkout master
```
to play. All instructions are in the README which should get printed
automatically by the hooks.


**NOTE** that git will also tell you you are in a detached head state. Just
ignore that message. The game content is under the bar of

```
======================
```

